# Amazon UI

A UI web application that interfaces with the REST API backend.

## Install Instructions

Make sure to have `node` and `yarn` installed before continuing.

Install dependencies.

```
$ yarn install
```

Run the server.

```
$ yarn start
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.
