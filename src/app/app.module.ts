import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { ProductsComponent } from './components/products/products.component';
import { LeftMenuComponent } from './components/left-menu/left-menu.component';
import { SearchAsinComponent } from './components/search-asin/search-asin.component';
import { MyProductListComponent } from './components/my-product-list/my-product-list.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    LeftMenuComponent,
    SearchAsinComponent,
    MyProductListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
