import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchAsinComponent } from './search-asin.component';

describe('SearchAsinComponent', () => {
  let component: SearchAsinComponent;
  let fixture: ComponentFixture<SearchAsinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchAsinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchAsinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
